import json
import sys

from PySide6.QtGui import QFontDatabase, QFont, QPixmap, Qt
from PySide6.QtWidgets import QMainWindow, QApplication, QVBoxLayout, QLabel, QWidget, QStackedWidget, QSizePolicy, \
    QHBoxLayout, QPushButton, QScrollArea, QLineEdit, QRadioButton


class Page(QWidget):
    def __init__(self, page_box, test_system):
        super().__init__()
        self.test_system = test_system
        self.page_box = page_box
        self.setLayout(QVBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

        self.question_title_box = QWidget()
        self.question_title_box.setLayout(QHBoxLayout())
        self.question_title_box.layout().setSpacing(0)
        self.question_title_box.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(self.question_title_box)

        self.question_title = QLabel("Вопрос:")
        self.question_title.setObjectName("question-title")
        self.question_title.setFixedWidth(76)
        self.question_title_box.layout().addWidget(self.question_title)

        self.question_text = QLabel("Вопрос?")
        self.question_text.setObjectName("question-text")
        self.question_text.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.question_text.setWordWrap(True)
        self.question_title_box.layout().addWidget(self.question_text)

        self.next_button = QPushButton(text='> Дальше')
        self.next_button.setObjectName('next-button')
        self.next_button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        self.question_title_box.layout().addWidget(self.next_button)
        self.next_button.clicked.connect(lambda: self.page_box.setCurrentIndex(self.page_box.currentIndex() + 1))

        self.question_content = QScrollArea()
        self.question_content.setLayout(QVBoxLayout())
        self.question_content.setObjectName("question-content")
        self.question_content.layout().setAlignment(Qt.AlignmentFlag.AlignTop)
        self.question_content.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.layout().addWidget(self.question_content)


class WelcomePage(Page):
    def __init__(self, page_box, test_system):
        super().__init__(page_box, test_system)
        self.question_text.setText("Перед началом тестирования, введите своё имя")

        self.name_input = QLineEdit(text=self.test_system.name)
        self.name_input.textChanged.connect(self._update_name)
        self.question_content.layout().addWidget(self.name_input)

        self.write_image = QLabel()
        self.write_image.setPixmap(QPixmap('./res/icons/write.svg'))
        self.question_content.layout().addWidget(self.write_image)

    def _update_name(self, text: str):
        self.test_system.name = text


class ResultPage(Page):
    def __init__(self, page_box, test_system):
        super().__init__(page_box, test_system)
        self.question_text.setText("Завершение тестирования")
        self.page_box.currentChanged.connect(self._update_results)

    def _update_results(self, index: int):
        if index == self.page_box.count() - 1:
            self.test_system.calculate_results()
            self.result_label = QLabel(
                f"Ваш результат: {self.test_system.correct_answers} из {len(self.test_system.data)}")
            self.question_content.layout().addWidget(self.result_label)


class PageBox(QStackedWidget):
    def __init__(self):
        super().__init__()
        self.setObjectName('page-box')
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)


class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.test_system = TestSystem()
        self.setWindowTitle("Система тестирования")
        self.main_widget = QWidget()
        self.main_layout = QVBoxLayout()
        self.main_widget.setLayout(self.main_layout)
        self.setCentralWidget(self.main_widget)
        self.setMinimumSize(800, 300)
        self.main_layout.setSpacing(0)
        self.main_layout.setContentsMargins(0, 0, 0, 0)

        # Подключение стилей
        with open('./styles/style.css') as style_file:
            self.setStyleSheet(style_file.read())

        # Свой шрифт
        QFontDatabase.addApplicationFont('./res/fonts/ClearSans-Medium.ttf')
        self.setFont(QFont('Clear Sans Medium'))

        self.title_box = QWidget()
        self.title_box.setObjectName("title-box")
        self.title_box.setLayout(QHBoxLayout())
        self.main_layout.addWidget(self.title_box)

        self.title_image = QLabel()
        self.title_image.setObjectName('title-image')
        self.title_image.setPixmap(QPixmap('./res/icons/question.svg'))
        self.title_image.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.title_box.layout().addWidget(self.title_image)

        self.title_text = QLabel("Тест «Автоматизированные обучающие системы»")
        self.title_text.setObjectName("title-text")
        self.title_box.layout().addWidget(self.title_text)

        self.page_box = PageBox()
        self.main_layout.addWidget(self.page_box)

        # Создание приветственной страницы
        self.page_box.addWidget(WelcomePage(self.page_box, self.test_system))

        # Создание страниц вопросов
        for n in range(len(self.test_system.data)):
            page = Page(self.page_box, self.test_system)
            page.question_text.setText(self.test_system.data[str(n + 1)]['Вопрос'])
            answers = self.test_system.data[str(n + 1)]['Варианты']
            for p, answer in answers.items():
                radio_button = QRadioButton(f"{p}) {answer}")
                radio_button.clicked.connect(self._add_answer)
                page.question_content.layout().addWidget(radio_button)
            self.page_box.addWidget(page)

        # Создание страницы результатов
        self.page_box.addWidget(ResultPage(self.page_box, self.test_system))

    def _add_answer(self):
        text = self.sender().text()
        self.test_system.user_answers[self.page_box.currentIndex() - 1] = text.split(")")[0]


class TestSystem:
    def __init__(self):
        with open("./data/questions.json", encoding='utf-8') as file:
            self.data = json.load(file)

        self.name = "Пользователь"
        self.user_answers = [None] * len(self.data)
        self.correct_answers = 0

    def calculate_results(self):
        for answer, question in zip(self.user_answers, self.data.values()):
            self.correct_answers += 1 if answer == question['Ответ'] else 0


if __name__ == '__main__':
    q_app = QApplication(sys.argv)
    window = Window()
    window.show()
    q_app.exec()
